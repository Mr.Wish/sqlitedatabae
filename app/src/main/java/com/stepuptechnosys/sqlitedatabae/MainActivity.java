package com.stepuptechnosys.sqlitedatabae;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class MainActivity extends Activity {
    SQLiteDatabase database;
    EditText eValue;
    Button btnInsert, btnShow, btn_update, btn_delete;
    TextView txtShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        eValue=findViewById(R.id.edit_value);
        btnInsert = findViewById(R.id.btn_insert);
        btnShow = findViewById(R.id.btn_show);
        txtShow = findViewById(R.id.txt_show);
        btn_update = findViewById(R.id.btn_update);
        btn_delete = findViewById(R.id.btn_delete);

        // Create databse
        database = this.openOrCreateDatabase("TestDb", MODE_PRIVATE, null);
        // Create table
        database.execSQL("CREATE TABLE IF NOT EXISTS TEST_TABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, PHONE TEXT)");

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String v1 = eValue.getText().toString();

                // Store Value
                ContentValues values = new ContentValues();
                values.put("NAME", v1);
                values.put("PHONE", "9876543210");

                database.insert("TEST_TABLE", null, values);

            }
        });

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tbName = "TEST_TABLE";
                // Store All Record from table
                Cursor c = database.query("TEST_TABLE", null, null, null, null, null, null);
                //Cursor c = database.rawQuery("SELECT * FROM " + tbName, null);
                // Cursur size
                int len = c.getCount();

                if (len != 0) {
                    c.moveToFirst();
                    for (int i = 0; i < len; i++) {
                        //txtShow.setText("NAME : " + c.getString(1)+ "     PHONE : " + c.getString(2) +"\n");

                        Log.d("NAME", c.getString(1) + "   " + c.getString(2));
                        Log.d("=====", "===================");

                        c.moveToNext();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No record", Toast.LENGTH_SHORT).show();
                }
                c.close();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String v1 = eValue.getText().toString();

                // Store Value
                ContentValues values = new ContentValues();
                values.put("NAME", "VISHAL");
                values.put("PHONE", "0000000000");

                database.update("TEST_TABLE", values, "ID=?", new String[]{v1});
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String v1 = eValue.getText().toString();

                database.delete("TEST_TABLE", "NAME=?", new String[]{v1});
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
